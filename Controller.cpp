#include "Controller.h"
#include <QMainWindow>
#include <QStringListModel>
#include <QStandardItemModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QDir>
#include "ui_main.h"
#include "StringIconListModel.h"
#include "PlayerProfile.h"
#include "PlayerComboBox.h"
#include "TeamTab.h"


PlayerInfoModel::PlayerInfoModel(QObject *parent, const QSqlDatabase &dbh) 
  : m_dbh(dbh),QSqlQueryModel(parent)
{
  setQuery(
    "SELECT PlayerInfo.name AS PlayerName, Team.name AS TeamName, PlayerInfo.Money AS PlayerMoney "
    "FROM PlayerInfo INNER JOIN Team ON Team.name=PlayerInfo.team",
    m_dbh
  );
}
bool PlayerInfoModel::setData(const QModelIndex &index, const QVariant &value, int role){
  QString queryString = "UPDATE ";
  switch (index.column()){
    case 0:
      queryString += "PlayerInfo SET name=? WHERE name=?";
      break;
    case 1:
      queryString += "Team SET name=? WHERE name=?";
      break;
    case 2:
      queryString += "PlayerInfo SET money=? WHERE name=?";
      break;
    default:
      return false;
  };
  QSqlQuery updateQuery(m_dbh);
  updateQuery.prepare(queryString);
  updateQuery.bindValue(0,value);
  updateQuery.bindValue(1,index.data());
  if (!updateQuery.exec()){
    printf("%s\n", updateQuery.lastError().text().toUtf8().data());
    return false;
  } else {
    query().exec();
    return true;
  }
}
Qt::ItemFlags PlayerInfoModel::flags(const QModelIndex &index) const {
  return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
}

Controller::Controller():
  m_main_window(new QMainWindow()),
  m_dbh(QSqlDatabase::addDatabase("QSQLITE","mainDb"))
{
  Ui::MainWindow ui;
  ui.setupUi(m_main_window.get());

  m_dbh.setDatabaseName(QDir::currentPath()+"/esports.sqlite");
  if (m_dbh.open())
    printf("successful connection\n");
  m_dbh.exec("pragma foreign_keys=on;");

  m_playerinfo_model.reset(new PlayerInfoModel(this, m_dbh));

  setup_players_tab();
  setup_team_tab();

  //m_main_window.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
  m_main_window->show();
}

bool Controller::setup_players_tab(){
  QSqlQuery query(m_dbh);
  query.prepare("SELECT name, country FROM Player ORDER BY name ASC");
  if (!query.exec())
    printf("DB doesn't exist\n");
  QWidget* playersTab = m_main_window->findChild<QWidget*>("playersTab");
  if (!playersTab)
    return false;
  QListView *playerList = playersTab->findChild<QListView*>("playerList");
  if (!playerList)
    return false;

  std::vector<QVariant> displays;
  std::vector<QVariant> flags;
  while (query.next()) {
    displays.push_back(query.value(0));
    QString flag_fn = QDir::currentPath()+"/flags/"+query.value(1).toString().toLower()+".png";
    flags.push_back(QIcon(flag_fn));
  }
  StringIconListModel *player_model = new StringIconListModel(displays,flags);
  playerList->setModel(player_model);

  PlayerProfile *playerProfile = m_main_window->findChild<PlayerProfile*>("playerProfile");
  if (!playerProfile)
    return false;

  QObject::connect(
    playerList, SIGNAL(clicked(const QModelIndex &)), 
    playerProfile, SLOT(update(const QModelIndex &))
  );
  QModelIndex root = player_model->index(0,0);
  if (root.isValid())
    playerProfile->update(root);
  
  return true;
}

bool Controller::setup_team_tab(){
  TeamTab* teamTab = m_main_window->findChild<TeamTab*>("teamTab");
  if (!teamTab){
    printf("Didn't find teamtab\n");
    return false;
  }

  teamTab->setPlayerInfoModel(m_playerinfo_model.get());
  teamTab->init();
}


