#ifndef _CONTROLLER_H
#define _CONTROLLER_H

#include <memory>
#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <Qt>

class PlayerInfoModel;
class Controller : public QObject {
  Q_OBJECT
  public:
    Controller();

    bool setup_players_tab();
    bool setup_team_tab();

  private:
    std::unique_ptr<QMainWindow> m_main_window;
    std::unique_ptr<PlayerInfoModel> m_playerinfo_model;
    QSqlDatabase m_dbh;
};

class PlayerInfoModel : public QSqlQueryModel {
  Q_OBJECT
  public:
    PlayerInfoModel(QObject *parent = NULL, const QSqlDatabase &dbh = QSqlDatabase());
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    Qt::ItemFlags flags(const QModelIndex &index) const;

  private:
    QSqlDatabase m_dbh;
};
#endif
