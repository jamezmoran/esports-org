#ifndef _PLAYER_THUMBNAIL_MODEL_H
#define _PLAYER_THUMBNAIL_MODEL_H

#include <vector>
#include <QAbstractListModel>
class StringIconListModel : public QAbstractListModel {
  Q_OBJECT
  public:
    StringIconListModel(const std::vector<QVariant> &displays, const std::vector<QVariant> &decorations, QObject *parent = NULL);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QModelIndex index(int row, int column=0, const QModelIndex & parent = QModelIndex());
    //QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

  private:
    std::vector<QVariant> m_displays;
    std::vector<QVariant> m_decorations;
};

#endif
