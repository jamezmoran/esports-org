#include <QApplication>
#include <QStyleFactory>
#include <cstdio>

#include "Controller.h"

int main(int argc, char **argv){
  QApplication qapp (argc, argv);

  Controller c;

  return qapp.exec();
}
