#ifndef _PLAYER_COMBO_BOX_H
#define _PLAYER_COMBO_BOX_H

#include <QWidget>
#include <QComboBox>

class PlayerComboBox : public QComboBox {
  Q_OBJECT
  public:
    PlayerComboBox(QWidget *parent = NULL) : QComboBox(parent) {}

  public slots:
    void updatePlayer(int index);
};

#endif
