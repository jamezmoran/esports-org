#ifndef _PLAYER_PROFILE_H
#define _PLAYER_PROFILE_H

#include <QWidget>
#include <QAbstractListModel>
class PlayerProfile : public QWidget {
  Q_OBJECT
  public:
    PlayerProfile(QWidget *parent = NULL);
  public slots:
    void update(const QModelIndex &index);
};

#endif
