#include "PlayerProfile.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QLabel>

// Player Profile 

PlayerProfile::PlayerProfile(QWidget* parent) : QWidget(parent) {
  hide();
}

void PlayerProfile::update(const QModelIndex &index){
  QString playerName = index.data(Qt::DisplayRole).toString();
  QSqlDatabase dbh = QSqlDatabase::database("mainDb");
  QSqlQuery query(dbh);
  query.prepare("SELECT name, age, country, gender, rank, description, reaction, accuracy, headshot, gamesense, tactics, support, riskiness, thumbnail FROM Player WHERE name=?");
  query.bindValue(0, playerName);
  if (query.exec() && query.next()){
    QList<QLabel*> playerNameLabel = findChildren<QLabel*>(QRegExp("^playerName(\_.*)?$"));
    if (!playerNameLabel.empty()){
      playerNameLabel[0]->setText(query.value(0).toString());
    }
    QList<QLabel*> playerInfoLabel = findChildren<QLabel*>(QRegExp("^playerInfo(\_.*)?$"));
    if (!playerInfoLabel.empty()){
      QString info = "Age: "+query.value(1).toString()+"\nCountry: "+query.value(2).toString().toUpper()+"\nGender: "+query.value(3).toString()+"\nRank: "+query.value(4).toString();
      playerInfoLabel[0]->setText(info);
    }
    QList<QLabel*> playerDescriptionLabel = findChildren<QLabel*>(QRegExp("^playerDescription(\_.*)?$"));
    if (!playerDescriptionLabel.empty()){
      playerDescriptionLabel[0]->setText(query.value(5).toString());
    }
    QList<QLabel*> statsValuesLabel = findChildren<QLabel*>(QRegExp("^statsValues(\_.*)?$"));
    if (!statsValuesLabel.empty()){
      int reaction = (query.value(6).toDouble()/10);
      int accuracy = (int)(query.value(7).toDouble()/10);
      int headshot = (int)(query.value(8).toDouble()/10);
      int gamesense = (int)(query.value(9).toDouble()/10);
      int tactics = (int)(query.value(10).toDouble()/10);
      int support = (int)(query.value(11).toDouble()/10);
      int aggression = (int)(query.value(12).toDouble()/10);
      QString values = "<html><head/><body><p>";
      for (int i = 0; i < 10; ++i){
        if (i > reaction)
          values += "░ ";
        else 
          values += "▓ ";
      }
      values += "<br>";
      for (int i = 0; i < 10; ++i){
        if (i > accuracy)
          values += "░ ";
        else 
          values += "▓ ";
      }
      values += "<br>";
      for (int i = 0; i < 10; ++i){
        if (i > headshot)
          values += "░ ";
        else 
          values += "▓ ";
      }
      values += "<br>";
      for (int i = 0; i < 10; ++i){
        if (i > gamesense)
          values += "░ ";
        else 
          values += "▓ ";
      }
      values += "<br>";
      for (int i = 0; i < 10; ++i){
        if (i > tactics)
          values += "░ ";
        else 
          values += "▓ ";
      }
      values += "<br>";
      for (int i = 0; i < 10; ++i){
        if (i > support)
          values += "░ ";
        else 
          values += "▓ ";
      }
      values += "<br>";
      for (int i = 0; i < 10; ++i){
        if (i > aggression)
          values += "░ ";
        else 
          values += "▓ ";
      }
      //</p><p>Accuracy:</p><p>Headshot:</p><p>Gamesense:</p><p>Tactics:</p></body></html>"
      values += "</p></body></html>";
      statsValuesLabel[0]->setText(values);
    }
    QList<QLabel*> playerPhotoLabel = findChildren<QLabel*>(QRegExp("^playerPhoto(\_.*)?$"));
    if (!playerPhotoLabel.empty()){
      QString thumb_fn;
      if (!query.value(13).isNull()){
        thumb_fn = QString("thumbs/")+query.value(13).toString();
      } else {
        thumb_fn = "thumbs/default.jpg";
      }
      playerPhotoLabel[0]->setPixmap(QPixmap(thumb_fn));
    }

  }
  show();
}

