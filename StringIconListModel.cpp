#include "StringIconListModel.h"

StringIconListModel::StringIconListModel(const std::vector<QVariant> &displays, const std::vector<QVariant> &decorations, QObject *parent) 
: m_displays(displays), m_decorations(decorations), QAbstractListModel(parent)
{
}
int StringIconListModel::rowCount(const QModelIndex &parent) const {
  return m_displays.size();
}
QModelIndex StringIconListModel::index(int row, int column, const QModelIndex & parent){
  if (row < m_displays.size())
    return createIndex(row,column,&m_displays[row]);
  return QModelIndex();
}
QVariant StringIconListModel::data(const QModelIndex &index, int role) const {
  if (role == Qt::DecorationRole){
    return m_decorations[index.row()];
  } else if (role == Qt::DisplayRole){
    return m_displays[index.row()];
  } 
  return QVariant();
}
