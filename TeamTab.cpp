#include "TeamTab.h"
#include <QLineEdit>
#include <QMessageBox>
#include <QStringListModel>
#include <QComboBox>
#include <QPushButton>
#include <QSqlQuery>
#include <QDir>
#include <QSqlRecord>
#include <QSqlField>
#include <QTableView>
#include <QListView>

#include "StringIconListModel.h"
#include "PlayerProfile.h"
#include "PlayerComboBox.h"


namespace {
  const QStringList roles = {"Wildcard","Entry Fragger","Rifler","Sniper","Ingame Leader","Support"};
}

TeamTab::TeamTab(QWidget* parent)
: QWidget(parent), m_playerinfo_model(NULL), m_roster_model(new QSqlTableModel(this, QSqlDatabase::database("mainDb")))
{
}
void TeamTab::init(){
  QListView *playerList = findChild<QListView*>("playerList_team");
  if (!playerList){
    printf("Didn't find playerlist\n");
    return;
  }

  std::vector<QVariant> displays;
  std::vector<QVariant> flags;
  std::vector<QVariant> thumbnails;
  QSqlQuery query(QSqlDatabase::database("mainDb"));
  query.prepare("SELECT Player.name, Player.country, Player.thumbnail FROM Team INNER JOIN Team_Player ON Team.name=Team_Player.team INNER JOIN Player ON Player.name=Team_Player.player WHERE team.player = 1 ORDER BY Player.name ASC");
  if (!query.exec())
    printf("DB doesn't exist\n");
  while (query.next()) {
    displays.push_back(query.value(0));
    QString flag_fn = QDir::currentPath()+"/flags/"+query.value(1).toString().toLower()+".png";
    QString thumb_fn;
    if (!query.value(2).isNull()){
      thumb_fn = QDir::currentPath()+"/thumbs/"+query.value(2).toString().toLower();
    } else {
      thumb_fn = QDir::currentPath()+"/thumbs/default.jpg";
    }

    flags.push_back(QIcon(flag_fn));
    thumbnails.push_back(QIcon(thumb_fn));
  }
  StringIconListModel *player_model = new StringIconListModel(displays,flags);
  playerList->setModel(player_model);

  PlayerProfile *playerProfile = findChild<PlayerProfile*>("playerProfile_team");
  if (!playerProfile){
    return;
  }

  QObject::connect(
    playerList, SIGNAL(clicked(const QModelIndex &)), 
    playerProfile, SLOT(update(const QModelIndex &))
  );

  QList<PlayerComboBox*> playerDropdowns = findChildren<PlayerComboBox*>(QRegExp("^playerDropdown[1-5]$"));
  std::vector<QVariant> dropdownDisplays = displays;
  std::vector<QVariant> dropdownThumbnails = thumbnails;
  dropdownDisplays.insert(dropdownDisplays.begin(),"---");
  dropdownThumbnails.insert(dropdownThumbnails.begin(),QIcon());

  StringIconListModel *playerDropdownModel = new StringIconListModel(dropdownDisplays, dropdownThumbnails, this);
  playerDropdownModel->insertRow(0);
  playerDropdownModel->setData(playerDropdownModel->index(0), "---", Qt::DisplayRole);
  for (PlayerComboBox* c : playerDropdowns){
    for (PlayerComboBox *nested_c : playerDropdowns){
      if (nested_c != c){
        QObject::connect(
          c, SIGNAL(currentIndexChanged(int)),
          nested_c, SLOT(updatePlayer(int))
        );
      }
    }
    c->setModel(playerDropdownModel);
  }
  QList<QComboBox*> roleDropdowns = findChildren<QComboBox*>(QRegExp("^roleDropdown[1-5]$"));
  QStringListModel *rolesModel = new QStringListModel(roles,this);
  if (!roleDropdowns.empty()){
    for (QComboBox *cb : roleDropdowns){
      cb->setModel(rolesModel);
    }
  }
  QString teamName = m_playerinfo_model->index(0,1).data().toString();
  teamName.replace('\'', "\\'");
  m_roster_model->setTable("Team_Player");
  m_roster_model->setFilter("team = '"+teamName+"' ORDER BY ordering");
  m_roster_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
  m_roster_model->select();
  for (int row = 0; row < m_roster_model->rowCount(); ++row){
    QString player = m_roster_model->index(row,1).data().toString();
    if (m_roster_model->index(row,2).data().isNull())
      continue;
    QString role = m_roster_model->index(row,2).data().toString();
    int ordering = m_roster_model->index(row,3).data().toInt();

    QAbstractItemModel *playersModel = playerDropdowns[ordering]->model();
    int roleIndex = rolesModel->match(rolesModel->index(0,0),Qt::DisplayRole,role)[0].row();
    int playerIndex = playersModel->match(playersModel->index(0,0),Qt::DisplayRole,player)[0].row();
    roleDropdowns[ordering]->setCurrentIndex(roleIndex);
    playerDropdowns[ordering]->setCurrentIndex(playerIndex);
  }
  QTableView *view = new QTableView();
  view->setModel(m_roster_model.get());
  view->show();
  QPushButton *submit = findChild<QPushButton*>("submitRoster");
  QPushButton *revert = findChild<QPushButton*>("revertRoster");
  if (submit && revert){
    QObject::connect(
      submit, SIGNAL(pressed()),
      this, SLOT(submitRoster())
    );
    QObject::connect(
      revert, SIGNAL(pressed()),
      this, SLOT(revertRoster())
    );
  }
}

void TeamTab::submitRoster(){
  QList<QComboBox*> playerDropdowns = findChildren<QComboBox*>(QRegExp("^playerDropdown[1-5]$"));
  QList<QComboBox*> roleDropdowns = findChildren<QComboBox*>(QRegExp("^roleDropdown[1-5]$"));

  if (playerDropdowns.empty() || roleDropdowns.empty()){
    printf("Could not find either players and/or role dropdowns\n");
    return;
  }
  QAbstractItemModel *model = playerDropdowns[0]->model();
  std::map<QString, QVariant> playersToRoles;
  std::map<QString, QVariant> playerOrdering;
  std::vector<QString> players(5);

  for (int row = 0; row < model->rowCount(); ++row){
    playersToRoles[model->index(row, 0).data().toString()] = QVariant();
    playerOrdering[model->index(row, 0).data().toString()] = QVariant();
  }

  QRegExp playerRegex("playerDropdown([1-5])");
  for (QComboBox *cb : playerDropdowns){
    if (cb->currentIndex()){ // null player should be at 0
      if (playerRegex.exactMatch(cb->objectName())){
        players[playerRegex.cap(1).toInt()-1] = cb->currentText();
      } 
    }
  }
  QRegExp roleRegex("roleDropdown([1-5])");
  for (QComboBox *cb : roleDropdowns){
    if (roleRegex.exactMatch(cb->objectName())){
      int boxNo = roleRegex.cap(1).toInt()-1;
      if (!players[boxNo].isEmpty()){
        playersToRoles[players[boxNo]] = cb->currentText();
        playerOrdering[players[boxNo]] = boxNo;
      }
    } 
  }

  for (int row = 0; row < m_roster_model->rowCount(); ++row){
    QSqlRecord record = m_roster_model->record(row);
    QString playerName = record.value(1).toString();
    record.setValue(2, playersToRoles[playerName]);
    record.setValue(3, playerOrdering[playerName]);

//    model->setData(model->index(row,2), playersToRoles[playerName]);
    m_roster_model->setRecord(row,record);
  }
  m_roster_model->submitAll();
  m_roster_model->select();

}
void TeamTab::revertRoster(){
  m_roster_model->revertAll();
}
void TeamTab::setPlayerInfoModel(PlayerInfoModel *model){
  m_playerinfo_model = model;
  QLineEdit *teamNameEdit = findChild<QLineEdit*>("teamNameEdit");
  if (teamNameEdit){
    teamNameEdit->setText(m_playerinfo_model->index(0,1).data().toString());
    QObject::connect(
      teamNameEdit, SIGNAL(editingFinished()),
      this, SLOT(updateTeamName())
    );
    QObject::connect(
      m_playerinfo_model, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)),
      this, SLOT(setTeamNameView(const QModelIndex&, const QModelIndex&, const QVector<int>&))
    );
  }
}

void TeamTab::updateTeamName(){
  QLineEdit *teamNameEdit = qobject_cast<QLineEdit*>(sender());
  QString name = teamNameEdit->text();
  if (!m_playerinfo_model->setData(m_playerinfo_model->index(0,1),name)){
    QMessageBox error;
    error.setText(QString("'")+name+"' already exists.");
    error.exec();
  }
}

void TeamTab::setTeamNameView(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles){
  if (topLeft == bottomRight && topLeft.column() == 1){
    emit teamNameUpdated(topLeft.data().toString());
  }
}

