#ifndef _TEAM_TAB_H
#define _TEAM_TAB_H

#include <QWidget>
#include <QVector>
#include <QModelIndex>
#include <QSqlTableModel>
#include "Controller.h"

class TeamTab : public QWidget {
  Q_OBJECT
  public:
    TeamTab(QWidget *parent = NULL);
    void setPlayerInfoModel(PlayerInfoModel *model);
    void init();

  public slots:
    void updateTeamName();
    void setTeamNameView(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>());
    void submitRoster();
    void revertRoster();

  signals:
    void teamNameUpdated(const QString& name);


  private:
    PlayerInfoModel *m_playerinfo_model;
    std::unique_ptr<QSqlTableModel> m_roster_model;
};

#endif
